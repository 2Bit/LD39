﻿using System;
using System.Text;

using UnityEngine;

public sealed class BranchStage : Stage
{
    [SerializeField]
    private Profile speaker = null;

    [SerializeField]
    private Option option1 = null;

    [SerializeField]
    private Option option2 = null;

    [SerializeField]
    private Option option3 = null;

    private Stage selectedStage = null;

    [SerializeField]
    private Stage outOfOptionsStage = null;

    public override void OnSetCurrent(Screen screen)
    {
        base.OnSetCurrent(screen);

        screen.SetOptions(
            this.speaker,
            option1.Stage == null ? "" : "1) " + option1.Text,
            () => this.selectedStage = option1.Stage,
            option2.Stage == null ? "" : "2) " + option2.Text,
            () => this.selectedStage = option2.Stage,
            option3.Stage == null ? "" : "3) " + option3.Text,
            () => this.selectedStage = option3.Stage);
    }

    public override void OnUnsetCurrent(Screen screen)
    {
        base.OnUnsetCurrent(screen);

        this.selectedStage = null;
    }

    public override Stage Process(Screen screen)
    {
        if (this.option1.Stage == null && this.option2.Stage == null && this.option3.Stage == null)
        {
            return this.outOfOptionsStage;
        }

        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            screen.FlushText();
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            this.selectedStage = option1.Stage;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            this.selectedStage = option2.Stage;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            this.selectedStage = option3.Stage;
        }

        if (this.selectedStage != null)
        {
            if (this.option1.Stage == this.selectedStage)
            {
                this.option1 = this.option2;
                this.option2 = this.option3;
            }
            else if (this.option2.Stage == this.selectedStage)
            {
                this.option2 = this.option3;
            }

            this.option3 = new Option();

            return this.selectedStage;
        }

        return this;
    }

    [Serializable]
    private sealed class Option
    {
        [SerializeField]
        private string text = null;

        [SerializeField]
        private Stage stage = null;

        public string Text
        {
            get
            {
                return this.text;
            }
        }

        public Stage Stage
        {
            get
            {
                return this.stage;
            }

            set
            {
                this.stage = value;
            }
        }
    }
}
