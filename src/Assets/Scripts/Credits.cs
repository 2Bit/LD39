﻿using UnityEngine;

public sealed class Credits : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup group = null;

    public void Quit()
    {
        Application.Quit();
    }

    private void Update()
    {
        this.group.alpha = Mathf.Clamp01(this.group.alpha + Time.deltaTime);
    }
}
