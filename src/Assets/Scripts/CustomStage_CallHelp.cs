﻿using System.Linq;

using UnityEngine;

public sealed class CustomStage_CallHelp : Stage
{
    [SerializeField]
    private ProfileCard cardPrefab = null;

    private ProfileCard topCard = null;
    private ProfileCard bottomCard = null;

    [SerializeField]
    private HubStage hub = null;

    private Profile[] contacts = null;

    [SerializeField]
    private Profile copyToProfile = null;

    [SerializeField]
    private Stage nextStage = null;

    private int contactIndex = 0;

    public override void OnSetCurrent(Screen screen)
    {
        base.OnSetCurrent(screen);

        screen.SetHubVisibility(true);

        this.contacts = this.hub.PreviousMatches.ToArray();

        if (this.contacts.Length > 0)
        {
            this.topCard = this.SpawnCard(this.contacts[this.contactIndex % this.contacts.Length], screen.HubContentRoot);

            this.bottomCard = this.SpawnCard(this.contacts[(this.contactIndex + 1) % this.contacts.Length], screen.HubContentRoot);
            this.bottomCard.IsDraggable = false;
        }
    }

    public override void OnUnsetCurrent(Screen screen)
    {
        base.OnUnsetCurrent(screen);

        for (int i = 0; i < screen.HubContentRoot.childCount; ++i)
        {
            GameObject.Destroy(screen.HubContentRoot.GetChild(i).gameObject);
        }

        screen.SetHubVisibility(false);
    }

    public override Stage Process(Screen screen)
    {
        if (this.topCard != null && this.topCard.IsTorn)
        {
            GameObject.Destroy(this.topCard.gameObject, 10.0f);

            if (this.topCard.transform.position.x > this.bottomCard.transform.position.x)
            {
                Profile copyFromProfile = this.contacts[this.contactIndex % this.contacts.Length];

                this.copyToProfile.Name = copyFromProfile.Name;
                this.copyToProfile.Image = copyFromProfile.Image;
                this.copyToProfile.Avatar = copyFromProfile.Avatar;
                this.copyToProfile.EyesOffset = copyFromProfile.EyesOffset;
                this.copyToProfile.Batteries = copyFromProfile.Batteries;

                return this.nextStage;
            }
            else
            {
                this.topCard = this.bottomCard;
                this.topCard.IsDraggable = true;

                ++this.contactIndex;

                this.bottomCard = this.SpawnCard(this.contacts[(this.contactIndex + 1) % this.contacts.Length], screen.HubContentRoot);
                this.bottomCard.IsDraggable = false;
            }
        }

        return this;
    }

    private ProfileCard SpawnCard(Profile profile, RectTransform cardRoot)
    {
        ProfileCard newCard = Component.Instantiate(this.cardPrefab);
        newCard.transform.SetParent(cardRoot);
        newCard.transform.localPosition = Vector3.zero;
        newCard.transform.SetAsFirstSibling();
        newCard.SetProfile(profile);

        return newCard;
    }
}
