﻿using System;

using UnityEngine;

public sealed class CustomStage_ChiefSparkr : Stage
{
    [SerializeField]
    private ProfileCard cardPrefab = null;
    private ProfileCard card = null;

    [SerializeField]
    private Profile chiefProfile = null;

    [SerializeField]
    private Stage nextStage = null;

    public override void OnSetCurrent(Screen screen)
    {
        base.OnSetCurrent(screen);

        screen.SetHubVisibility(true);

        this.card = Component.Instantiate(this.cardPrefab);
        this.card.transform.SetParent(screen.HubContentRoot);
        this.card.transform.localPosition = Vector3.zero;
        this.card.transform.SetAsFirstSibling();
        this.card.SetProfile(this.chiefProfile);
        this.card.IsDraggable = false;
    }

    public override void OnUnsetCurrent(Screen screen)
    {
        base.OnUnsetCurrent(screen);

        for (int i = 0; i < screen.HubContentRoot.childCount; ++i)
        {
            GameObject.Destroy(screen.HubContentRoot.GetChild(i).gameObject);
        }

        screen.SetHubVisibility(false);
    }

    public override Stage Process(Screen screen)
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            return this.nextStage;
        }

        return this;
    }
}
