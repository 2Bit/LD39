﻿using UnityEngine;

public sealed class DialogStage : Stage
{
    [SerializeField]
    private Profile speaker = null;

    [SerializeField]
    [TextArea]
    private string text = null;

    [SerializeField]
    private Sprite background = null;

    [SerializeField]
    private Profile right = null;

    [SerializeField]
    private Screen.EyeType rightEyes = Screen.EyeType.None;

    [SerializeField]
    private Color rightColor = Color.white;

    [SerializeField]
    private Profile left = null;

    [SerializeField]
    private Screen.EyeType leftEyes = Screen.EyeType.None;

    [SerializeField]
    private Color leftColor = Color.white;

    [SerializeField]
    private Profile center = null;

    [SerializeField]
    private Screen.EyeType centerEyes = Screen.EyeType.None;

    [SerializeField]
    private Color centerColor = Color.white;

    [SerializeField]
    private Stage nextStage = null;

    public override void OnSetCurrent(Screen screen)
    {
        base.OnSetCurrent(screen);

        screen.SetText(this.speaker, this.text);
        screen.SetBackground(this.background);
        screen.SetRight(this.right, this.rightEyes, this.rightColor);
        screen.SetLeft(this.left, this.leftEyes, this.leftColor);
        screen.SetCenter(this.center, this.centerEyes, this.centerColor);
    }

    public override Stage Process(Screen screen)
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            if (screen.IsWritingText)
            {
                screen.FlushText();
            }
            else
            {
                return this.nextStage;
            }
        }

        return this;
    }
}
