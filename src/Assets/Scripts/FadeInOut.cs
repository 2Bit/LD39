﻿using UnityEngine;
using UnityEngine.UI;

using Slerpy.Unity3D;

public sealed class FadeInOut : MonoBehaviour
{
    [SerializeField]
    private Ghoster ghoster = null;

    private void LateUpdate()
    {
        Image[] images = this.gameObject.GetComponentsInChildren<Image>();
        for (int i = 0; i < images.Length; ++i)
        {
            if (images[i].sprite != null)
            {
                images[i].color = new Color(
                    images[i].color.r,
                    images[i].color.g,
                    images[i].color.b,
                    Mathf.Clamp01(images[i].color.a + (this.ghoster.IsGhost ? -Time.deltaTime : Time.deltaTime)));
            }
        }
    }
}
