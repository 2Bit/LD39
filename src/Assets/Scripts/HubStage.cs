﻿using System;
using System.Collections.Generic;

using UnityEngine;

public sealed class HubStage : Stage
{
    [SerializeField]
    private ProfileCard cardPrefab = null;

    private ProfileCard topCard = null;
    private ProfileCard bottomCard = null;

    [SerializeField]
    private List<MatchedProfile> matches = null;

    [SerializeField]
    private Profile requiredMatchBeforeFinal = null;

    [SerializeField]
    private MatchedProfile finalMatch = null;

    private int matchIndex = 0;

    private readonly List<Profile> previousMatches = new List<Profile>();

    public IEnumerable<Profile> PreviousMatches
    {
        get
        {
            return this.previousMatches;
        }
    }

    public bool HasCurrentMatch(Profile profile)
    {
        for (int i = 0; i < this.matches.Count; ++i)
        {
            if (this.matches[i].Profile == profile)
            {
                return true;
            }
        }

        return false;
    }

    public override void OnSetCurrent(Screen screen)
    {
        base.OnSetCurrent(screen);

        screen.SetHubVisibility(true);

        if (this.matches.Count <= 3 && !this.matches.Contains(this.finalMatch))
        {
            bool requiredMatchCompleted = true;

            for (int i = 0; i < this.matches.Count; ++i)
            {
                if (this.matches[i].Profile == this.requiredMatchBeforeFinal)
                {
                    requiredMatchCompleted = false;

                    break;
                }
            }

            if (requiredMatchCompleted)
            {
                this.matches.Add(finalMatch);
            }
        }

        this.topCard = this.SpawnCard(this.matches[this.matchIndex % this.matches.Count].Profile, screen.HubContentRoot);

        this.bottomCard = this.SpawnCard(this.matches[(this.matchIndex + 1) % this.matches.Count].Profile, screen.HubContentRoot);
        this.bottomCard.IsDraggable = false;
    }

    public override void OnUnsetCurrent(Screen screen)
    {
        base.OnUnsetCurrent(screen);

        for (int i = 0; i < screen.HubContentRoot.childCount; ++i)
        {
            GameObject.Destroy(screen.HubContentRoot.GetChild(i).gameObject);
        }

        screen.SetHubVisibility(false);
    }

    public override Stage Process(Screen screen)
    {
        if (this.topCard.IsTorn)
        {
            GameObject.Destroy(this.topCard.gameObject, 10.0f);

            if (this.topCard.transform.position.x > this.bottomCard.transform.position.x)
            {
                MatchedProfile match = this.matches[this.matchIndex % this.matches.Count];

                this.matches.RemoveAt(this.matchIndex % this.matches.Count);

                if (match != this.finalMatch)
                {
                    this.previousMatches.Add(match.Profile);
                }

                return match.Stage;
            }
            else
            {
                this.topCard = this.bottomCard;
                this.topCard.IsDraggable = true;

                ++this.matchIndex;

                this.bottomCard = this.SpawnCard(this.matches[(this.matchIndex + 1) % this.matches.Count].Profile, screen.HubContentRoot);
                this.bottomCard.IsDraggable = false;
            }
        }

        return this;
    }

    public void AddMatch(Profile profile, Stage stage)
    {
        if (((this.matchIndex + 1) % this.matches.Count) > (this.matchIndex % this.matches.Count))
        {
            ++this.matchIndex;
        }

        this.matches.Add(new MatchedProfile(profile, stage));
    }

    private ProfileCard SpawnCard(Profile profile, RectTransform cardRoot)
    {
        ProfileCard newCard = Component.Instantiate(this.cardPrefab);
        newCard.transform.SetParent(cardRoot);
        newCard.transform.localPosition = Vector3.zero;
        newCard.transform.SetAsFirstSibling();
        newCard.SetProfile(profile);

        return newCard;
    }

    [Serializable]
    private sealed class MatchedProfile
    {
        [SerializeField]
        private Profile profile = null;

        [SerializeField]
        private Stage stage = null;

        public Profile Profile
        {
            get
            {
                return this.profile;
            }
        }

        public Stage Stage
        {
            get
            {
                return this.stage;
            }
        }

        public MatchedProfile()
        {
        }

        public MatchedProfile(Profile profile, Stage stage)
        {
            this.profile = profile;
            this.stage = stage;
        }
    }
}
