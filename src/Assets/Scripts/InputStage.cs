﻿using System;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public sealed class InputStage : Stage
{
    [SerializeField]
    private ContinueEvent onContinue = null;

    [SerializeField]
    private int maxInputLength = 0;

    [SerializeField]
    private InputField.ContentType inputType = InputField.ContentType.Standard;

    [SerializeField]
    private bool multiline = false;

    [SerializeField]
    private string instructions = null;

    [SerializeField]
    private Stage nextStage = null;

    public override void OnSetCurrent(Screen screen)
    {
        base.OnSetCurrent(screen);

        screen.SetHubVisibility(true);
        screen.SetHubInput(true, this.instructions);

        screen.HubInputField.characterLimit = this.maxInputLength;
        screen.HubInputField.contentType = this.inputType;

        Vector2 sizeDelta = ((RectTransform)screen.HubInputField.transform).sizeDelta;
        sizeDelta.y *= this.multiline ? 2.0f : 1.0f;
        ((RectTransform)screen.HubInputField.transform).sizeDelta = sizeDelta;
        screen.HubInputField.lineType = this.multiline ? InputField.LineType.MultiLineSubmit : InputField.LineType.SingleLine;
        screen.HubInputField.textComponent.resizeTextForBestFit = !this.multiline;
    }

    public override void OnUnsetCurrent(Screen screen)
    {
        base.OnUnsetCurrent(screen);

        Vector2 sizeDelta = ((RectTransform)screen.HubInputField.transform).sizeDelta;
        sizeDelta.y *= this.multiline ? 0.5f : 1.0f;
        ((RectTransform)screen.HubInputField.transform).sizeDelta = sizeDelta;
        screen.HubInputField.lineType = InputField.LineType.SingleLine;
        screen.HubInputField.textComponent.resizeTextForBestFit = true;

        screen.SetHubInput(false, "");
        screen.SetHubVisibility(false);
    }

    public override Stage Process(Screen screen)
    {
        if (Input.GetKeyDown(KeyCode.Return) && screen.HubInputField.text.Trim().Length > 0)
        {
            this.onContinue.Invoke(screen.HubInputField.text);

            return this.nextStage;
        }

        return this;
    }

    [Serializable]
    private sealed class ContinueEvent : UnityEvent<string> { }
}
