﻿using UnityEngine;

using Slerpy.Unity3D;

public sealed class Menu : MonoBehaviour
{
    public void LoadLevel()
    {
        Application.LoadLevel("Main");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
