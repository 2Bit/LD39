﻿using UnityEngine;

public sealed class Profile : MonoBehaviour
{
    [SerializeField]
    private new string name = null;

    [SerializeField]
    private Sprite image = null;

    [SerializeField]
    private Sprite avatar = null;

    [SerializeField]
    private Vector2 eyesOffset = Vector2.zero;

    [SerializeField]
    private string batteries = null;

    public string Name
    {
        get
        {
            return this.name;
        }

        set
        {
            this.name = value;
        }
    }

    public Sprite Image
    {
        get
        {
            return this.image;
        }

        set
        {
            this.image = value;
        }
    }

    public Sprite Avatar
    {
        get
        {
            return this.avatar;
        }

        set
        {
            this.avatar = value;
        }
    }

    public Vector2 EyesOffset
    {
        get
        {
            return this.eyesOffset;
        }

        set
        {
            this.eyesOffset = value;
        }
    }

    public string Batteries
    {
        get
        {
            return this.batteries;
        }

        set
        {
            this.batteries = value;
        }
    }
}
