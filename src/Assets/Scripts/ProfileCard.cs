﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public sealed class ProfileCard : MonoBehaviour
{
    private const float MAX_Y = 50.0f;
    private const float MAX_X = 200.0f;

    [SerializeField]
    private new Text name = null;

    [SerializeField]
    private Image image = null;

    private Vector3 tetherPosition = Vector3.zero;

    public bool IsDraggable { get; set; }

    public bool IsDragging { get; private set; }
    public bool IsTorn { get; private set; }

    public void SetProfile(Profile profile)
    {
        this.name.text = string.IsNullOrEmpty(profile.Batteries) ? profile.Name : string.Concat(profile.Name, ", ", profile.Batteries);
        this.image.sprite = profile.Avatar == null ? profile.Image : profile.Avatar;
    }

    public void OnBeginDrag(UnityEngine.EventSystems.BaseEventData eventData)
    {
        this.IsDragging = this.IsDraggable && !this.IsTorn;
    }

    public void OnDrag(UnityEngine.EventSystems.BaseEventData eventData)
    {
        if (this.IsDragging)
        {
            UnityEngine.EventSystems.PointerEventData pointerData = eventData as UnityEngine.EventSystems.PointerEventData;
            if (pointerData != null)
            {
                Vector3 newPosition = this.transform.localPosition + new Vector3(pointerData.delta.x, pointerData.delta.y, 0.0f);

                if (newPosition.y - this.tetherPosition.y > ProfileCard.MAX_Y)
                {
                    newPosition.y = this.tetherPosition.y + ProfileCard.MAX_Y;
                }
                else if (newPosition.y - this.tetherPosition.y < -ProfileCard.MAX_Y)
                {
                    newPosition.y = this.tetherPosition.y - ProfileCard.MAX_Y;
                }

                this.transform.localPosition = newPosition;
            }
        }
    }

    public void OnEndDrag(UnityEngine.EventSystems.BaseEventData eventData)
    {
        this.IsDragging = false;
    }

    private void Awake()
    {
        this.IsDraggable = true;
    }

    private void Start()
    {
        this.tetherPosition = this.transform.localPosition;
    }

    private void Update()
    {
        if (Mathf.Abs(this.transform.localPosition.x - this.tetherPosition.x) > ProfileCard.MAX_X)
        {
            this.IsDragging = false;
            this.IsTorn = true;
        }

        if (this.IsTorn)
        {
            this.transform.localPosition += (this.transform.localPosition - this.tetherPosition).normalized * Time.deltaTime * 400.0f;
        }
        else
        {
            if (!this.IsDragging)
            {
                this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, this.tetherPosition, Time.deltaTime * 5.0f);
            }
        }

        this.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.5f * (this.transform.localPosition.y - this.tetherPosition.y) * ((this.transform.localPosition.x - this.tetherPosition.x) / ProfileCard.MAX_X));
    }
}
