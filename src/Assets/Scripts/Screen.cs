﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

using Slerpy.Unity3D;

public sealed class Screen : MonoBehaviour
{
    public enum EyeType
    {
        None = -1,
        Normal = 0,
        Happy = 1,
        Angry = 2,
        Scared = 3,
        Sigh = 4,
        Dead = 5,
        Love = 6,
        Wink = 7,
        Sad = 8,
        Furious = 9,
        Shocked = 10,
        Greaser = 11,
        Blush = 12,
    }

    [SerializeField]
    private Profile playerProfile = null;

    [SerializeField]
    private Canvas canvas = null;

    [SerializeField]
    private VideoPlayer videoPlayer = null;

    [SerializeField]
    private AudioSource musicFalloffSource = null;

    [SerializeField]
    private AudioSource musicSource = null;

    [SerializeField]
    private AudioSource writingSource = null;

    [SerializeField]
    private Effect hubBackgroundEffect = null;

    [SerializeField]
    private Effect hubVisibilityEffect = null;

    [SerializeField]
    private Effect hubRotateEffect = null;

    [SerializeField]
    private RectTransform hubContentRoot = null;

    [SerializeField]
    private InputField hubInputField = null;

    [SerializeField]
    private Text hubInputInstructions = null;

    [SerializeField]
    private Image background = null;

    [SerializeField]
    private Ghoster backgroundGhoster = null;

    [SerializeField]
    private ProfileDrawer right = null;
    private EyeType rightEyeType = EyeType.None;

    [SerializeField]
    private ProfileDrawer left = null;
    private EyeType leftEyeType = EyeType.None;

    [SerializeField]
    private ProfileDrawer center = null;
    private EyeType centerEyeType = EyeType.None;

    [SerializeField]
    private RectTransform[] eyesPrefabs = null;

    [SerializeField]
    private new AnimatedText name = null;

    [SerializeField]
    private AnimatedText text = null;

    [SerializeField]
    private Button option1Button = null;

    [SerializeField]
    private AnimatedText option1Text = null;

    [SerializeField]
    private Button option2Button = null;

    [SerializeField]
    private AnimatedText option2Text = null;

    [SerializeField]
    private Button option3Button = null;

    [SerializeField]
    private AnimatedText option3Text = null;

    [SerializeField]
    private GameObject prompt = null;

    private readonly List<Type> ghosterPreservedTypes = new List<Type>();

    public RectTransform HubContentRoot
    {
        get
        {
            return this.hubContentRoot;
        }
    }

    public InputField HubInputField
    {
        get
        {
            return this.hubInputField;
        }
    }

    public bool IsWritingText
    {
        get
        {
            return !(this.text.AnimationTime >= this.text.text.Length * this.text.Interval * 4.0f + this.text.Duration);
        }
    }

    public bool IsPlayingVideo
    {
        get
        {
            return this.videoPlayer.isPlaying;
        }
    }

    public void PlayVideo(VideoClip video)
    {
        this.videoPlayer.clip = video;
        this.videoPlayer.Play();
    }

    public void SetMusic(AudioClip clip)
    {
        if (this.musicSource.clip != clip)
        {
            this.musicFalloffSource.clip = this.musicSource.clip;
            this.musicFalloffSource.time = this.musicSource.time;
            this.musicFalloffSource.volume = 1.0f;
            this.musicFalloffSource.Play();
        
            this.musicSource.clip = clip;
            this.musicSource.volume = 0.0f;
            this.musicSource.Play();
        }
    }

    public void SetHubVisibility(bool visible)
    {
        if (visible)
        {
            this.hubVisibilityEffect.PlayForward();
            this.hubBackgroundEffect.PlayForward();
        }
        else
        {
            this.hubVisibilityEffect.PlayBackward();
            this.hubBackgroundEffect.PlayBackward();
        }
    }

    public void SetHubInput(bool isActive, string instructions)
    {
        this.hubInputField.text = "";
        this.hubInputField.gameObject.SetActive(isActive);
        this.hubInputField.Select();

        this.hubInputInstructions.text = instructions;

        if (isActive)
        {
            this.hubRotateEffect.PlayForward();
        }
        else
        {
            this.hubRotateEffect.PlayBackward();
        }
    }

    public void SetBackground(Sprite sprite)
    {
        this.SetSprite(this.background, this.backgroundGhoster, sprite, Color.white);
    }

    public void SetRight(Profile profile, EyeType eyeType, Color tint)
    {
        if (this.rightEyeType != eyeType || profile == null || this.right.Body.sprite != profile.Image)
        {
            this.SetEyes(this.right.EyesRoot, profile == null ? Vector2.zero : profile.EyesOffset, this.rightEyeType = eyeType);
        }

        this.SetSprite(this.right.Body, this.right.Ghoster, profile == null ? null : profile.Image, tint);
    }

    public void SetLeft(Profile profile, EyeType eyeType, Color tint)
    {
        if (this.leftEyeType != eyeType || profile == null || this.left.Body.sprite != profile.Image)
        {
            this.SetEyes(this.left.EyesRoot, profile == null ? Vector2.zero : profile.EyesOffset, this.leftEyeType = eyeType);
        }

        this.SetSprite(this.left.Body, this.left.Ghoster, profile == null ? null : profile.Image, tint);
    }

    public void SetCenter(Profile profile, EyeType eyeType, Color tint)
    {
        if (this.centerEyeType != eyeType || profile == null || this.center.Body.sprite != profile.Image)
        {
            this.SetEyes(this.center.EyesRoot, profile == null ? Vector2.zero : profile.EyesOffset, this.centerEyeType = eyeType);
        }

        this.SetSprite(this.center.Body, this.center.Ghoster, profile == null ? null : profile.Image, tint);
    }

    public void FlushText()
    {
        this.text.AnimationTime = this.option1Text.AnimationTime = this.option2Text.AnimationTime = this.option3Text.AnimationTime = 1000.0f;
    }

    public void SetText(Profile profile, string text)
    {
        this.SetProfile(profile);

        this.text.gameObject.SetActive(true);
        this.option1Button.gameObject.SetActive(false);
        this.option2Button.gameObject.SetActive(false);
        this.option3Button.gameObject.SetActive(false);

        this.text.text = this.ParseString(text);
        this.text.Rewind();
    }

    public void SetOptions(Profile profile, string option1Text, Action option1Action, string option2Text, Action option2Action, string option3Text, Action option3Action)
    {
        this.SetProfile(profile);

        this.text.gameObject.SetActive(false);
        this.option1Button.gameObject.SetActive(true);
        this.option2Button.gameObject.SetActive(true);
        this.option3Button.gameObject.SetActive(true);

        this.option1Text.text = this.ParseString(option1Text);
        this.option1Text.Rewind();
        this.option1Button.onClick.RemoveAllListeners();
        this.option1Button.onClick.AddListener(() => option1Action.Invoke());
        this.option1Button.interactable = !string.IsNullOrEmpty(option1Text);

        this.option2Text.text = this.ParseString(option2Text);
        this.option2Text.Rewind();
        this.option2Button.onClick.RemoveAllListeners();
        this.option2Button.onClick.AddListener(() => option2Action.Invoke());
        this.option2Button.interactable = !string.IsNullOrEmpty(option2Text);

        this.option3Text.text = this.ParseString(option3Text);
        this.option3Text.Rewind();
        this.option3Button.onClick.RemoveAllListeners();
        this.option3Button.onClick.AddListener(() => option3Action.Invoke());
        this.option3Button.interactable = !string.IsNullOrEmpty(option3Text);
    }

    private void SetProfile(Profile profile)
    {
        if (this.name.text != profile.Name)
        {
            this.name.text = profile.Name;
            this.name.Rewind();
        }
    }

    private void SetSprite(Image target, Ghoster ghoster, Sprite sprite, Color tint)
    {
        if (target.sprite != sprite || target.color.r != tint.r || target.color.g != tint.g || target.color.b != tint.b)
        {
            Ghoster ghost = ghoster.SpawnTemporary(5.0f, this.ghosterPreservedTypes);
            ghost.transform.SetParent(target.transform.parent);
            ghost.transform.SetSiblingIndex(target.transform.GetSiblingIndex());
            ((RectTransform)ghost.transform).sizeDelta = Vector2.zero;

            target.sprite = sprite;
            target.color = new Color(tint.r, tint.g, tint.b, 0.0f);
            target.preserveAspect = true;
        }
    }

    private void SetEyes(RectTransform eyesRoot, Vector2 offset, EyeType type)
    {
        for (int i = 0; i < eyesRoot.childCount; ++i)
        {
            GameObject.Destroy(eyesRoot.GetChild(i).gameObject);
        }

        if (type != EyeType.None)
        {
            RectTransform newEyes = Component.Instantiate(this.eyesPrefabs[(int)type]);
            newEyes.SetParent(eyesRoot);
            newEyes.anchoredPosition = Vector2.zero;
            newEyes.sizeDelta = Vector2.zero;

            ((RectTransform)eyesRoot.transform).anchorMax = Vector2.one + offset;
            ((RectTransform)eyesRoot.transform).anchorMin = Vector2.zero + offset;
        }
    }

    private string ParseString(string original)
    {
        return original.Replace("<name>", string.Concat("<color=#ff0000>", this.playerProfile.Name, "</color>"));
    }

    private void Awake()
    {
        this.ghosterPreservedTypes.AddRange(Ghoster.DefaultPreservedTypes);
        this.ghosterPreservedTypes.Add(typeof(Graphic));
        this.ghosterPreservedTypes.Add(typeof(CanvasRenderer));
        this.ghosterPreservedTypes.Add(typeof(FadeInOut));
    }

    private void LateUpdate()
    {
        this.musicFalloffSource.volume = Mathf.Clamp01(this.musicFalloffSource.volume - Time.deltaTime);
        this.musicSource.volume = Mathf.Clamp01(this.musicSource.volume + Time.deltaTime);

        this.prompt.SetActive(this.text.gameObject.activeInHierarchy && !this.IsWritingText);
        this.writingSource.enabled = this.IsWritingText;

        this.musicFalloffSource.enabled = this.musicSource.enabled = this.canvas.enabled = !this.IsPlayingVideo;
    }

    [Serializable]
    private sealed class ProfileDrawer
    {
        [SerializeField]
        private Image body = null;

        [SerializeField]
        private RectTransform eyesRoot = null;

        [SerializeField]
        private Ghoster ghoster = null;

        public Image Body
        {
            get
            {
                return this.body;
            }
        }

        public RectTransform EyesRoot
        {
            get
            {
                return this.eyesRoot;
            }
        }

        public Ghoster Ghoster
        {
            get
            {
                return this.ghoster;
            }
        }
    }
}
