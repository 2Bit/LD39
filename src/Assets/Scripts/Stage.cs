﻿using UnityEngine;
using UnityEngine.Events;

public abstract class Stage : MonoBehaviour
{
    [SerializeField]
    private AudioClip music = null;

    [SerializeField]
    private AudioClip soundEffect = null;

    public virtual void OnSetCurrent(Screen screen)
    {
        screen.SetMusic(this.music);

        if (this.soundEffect != null)
        {
            AudioSource.PlayClipAtPoint(this.soundEffect, Camera.main.transform.position, 1.0f);
        }
    }

    public virtual void OnUnsetCurrent(Screen screen) { }

    public abstract Stage Process(Screen screen);
}
