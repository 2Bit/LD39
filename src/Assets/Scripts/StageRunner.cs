﻿using UnityEngine;

public sealed class StageRunner : MonoBehaviour
{
    [SerializeField]
    private Screen screen = null;

    [SerializeField]
    private Stage startingStage = null;

    [SerializeField]
    private RectTransform credits = null;

    public Stage CurrentStage
    {
        get
        {
            return this.currentStage;
        }

        set
        {
            if (this.currentStage != value)
            {
                if (this.currentStage != null)
                {
                    this.currentStage.OnUnsetCurrent(this.screen);
                }

                this.currentStage = value;

                if (this.currentStage != null)
                {
                    this.currentStage.OnSetCurrent(this.screen);
                }
            }
        }
    }

    private Stage currentStage = null;

    private void Start()
    {
        this.CurrentStage = this.startingStage;
    }

    private void Update()
    {
        if (this.CurrentStage != null)
        {
            this.CurrentStage = this.CurrentStage.Process(this.screen);
        }
        else
        {
            this.credits.gameObject.SetActive(true);
        }
    }
}
