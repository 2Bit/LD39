﻿using UnityEngine;
using UnityEngine.Video;

public sealed class VideoStage : Stage
{
    [SerializeField]
    private VideoClip video = null;

    [SerializeField]
    private Stage nextStage = null;

    public override void OnSetCurrent(Screen screen)
    {
        base.OnSetCurrent(screen);

        screen.PlayVideo(this.video);
        screen.SetBackground(null);
        screen.SetRight(null, Screen.EyeType.None, Color.white);
        screen.SetLeft(null, Screen.EyeType.None, Color.white);
        screen.SetCenter(null, Screen.EyeType.None, Color.white);
    }

    public override Stage Process(Screen screen)
    {
        return screen.IsPlayingVideo ? this : this.nextStage;
    }
}
